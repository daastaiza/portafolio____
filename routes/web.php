<?php


Route::get('/', function () {
    return view('inicio');
});

 Route::resource('/portafolio', 'Publicacion\ControllerPublicacion@general');

 Route::resource('/index', 'Index\IndexController');
  Route::resource('/aportes', 'Aprotes\AportesController@aportes');
 Route::resource('/estudios', 'Index\IndexController@estudios');
 Route::resource('/informacion', 'Index\IndexController@informacion');

 

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'Administrador'], function () {

    Route::resource('Ciudad', 'Ciudad\CiudadController');

    Route::resource('Informacion', 'Informacion\InformacionController');

    Route::resource('Categoria', 'Categoria\categoriaController');

    Route::resource('Portafolio', 'Portafolio\PortafioController');

    Route::resource('Secciones', 'Secciones\ControllerSecciones');

    Route::resource('Publicaciones', 'Publicacion\ControllerPublicacion');

    Route::resource('Estudios', 'Estudios\EstudiosController');

    Route::resource('Aportes', 'Aprotes\AportesController');

});
