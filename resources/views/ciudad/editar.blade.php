@extends('layouts.app')

@section('content')
<div class="container">
    {!! Form::open(['route' => ['Ciudad.update',$ciudad->id], 'method' => 'PUT']) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('nombre', $ciudad->nombre, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la peticion']) !!}
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                    </div>


@endsection
