@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        
                <div class="panel-heading">
                    <div class="container">
                        <div class="container">
                    <h2>Publicaciones</h2> 
                    </div>   
                    </div>
                
            </div>

                <div class="panel-body">
                    <div class="container-fluid">
                        <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h3 class="panel-title"><i class="fa fa-list"></i>
    <div class="container">
                  Publicaciones realizadas
              </div>
              </h3>
                </div>
                <div class="card-block">

                            <div class="panel-body">
                                @if(count($errors) >0)
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                @include('flash::message')

                                <div class="table-responsive">
                                   <table class="table table-hover table-sm table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Identificador</th>
                                            <th>Baner</th>
                                            <th>Titulo</th>
                                            <th>Contenido</th>
                                            <th>Seccion</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($publicacion  as $publicacion)
                                        <tr>
                                                <td>{{ $publicacion -> id }}</td>
                                                <td><img src="/storage/{{ $publicacion -> image }}" width="100px"></td>
                                                <td>{{ $publicacion -> titulo}}</td>
                                                <td>{{ $publicacion -> contenido}}</td>
                                                <td>{{ $publicacion -> seccion -> nombre}}</td>
                                        </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <a ><button type="button" class="btn btn-success" data-toggle="modal"   data-target="#Crear">Nueva <i class="fa fa-fw fa-plus"></i></button></a><br><br>
                                </div>
                            </div>
                    </div>
                </div>
                
                </div>
                </div>
                </div>

                
                </div>

  <div id="Crear" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                        <!-- Modal content-->
                                    <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">gestion de publicaciones</h4>
                                          </div>
                                          <div class="modal-body">
                                        {!! Form::open(['route' => 'Publicaciones.store', 'method' => 'POST','files' => true]) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Titulo') !!}
                                            {!! Form::text('titulo', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'titulo de la publicacion']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('description', 'Contenido') !!}
                                            {!! Form::textarea('contenido', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'contenido del contenido']) !!}
                                        </div>
                                         <div class="form-group">
                                            {!! Form::label('name', 'Seccion') !!}

                                            {!! Form::select('id_seccion', $secciones, null, ['class' => 'form-control priority-select ', 'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('name', 'Seleccione una imagen') !!}

                                            {!! Form::file('image') !!}
                                        </div>
                                          {!! Form::hidden('id_user',Auth::user()->id, ['class' =>'']) !!}


                                        <div class="modal-footer">
                                            {!! Form::submit('registrar')!!}
                                        </div>

                                        {!! Form::close() !!}
                                        </div>

                                    </div>
                                </div>
                            </div>



@endsection
