<!DOCTYPE HTML>
<!--
    Caminar by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
    <head>
        <title>Informacion Personal</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="{{asset('assets/css/main_information.css')}}" />
    </head>
    <body>

        <!-- Header -->
            <header id="header">
                <div class="logo"><a href="#">Informacion Personal <span>Diego Alejandro Astaiza Borja</span></a></div>
            </header>

        <!-- Main -->
            <section id="main">
                <div class="inner">

                @foreach ($informacion as $informacion)
                                       
                                           
                    <section id="one" class="wrapper style1">

                        <div class="image fit flush">
                            <img src="/storage/{{ $informacion -> image }}" alt="" />
                        </div>
                        <header class="special">
                            <h2>{{ $informacion -> nombre}}</h2>
                            <p>Aprendiz SENA</p>
                        </header>
                        <div class="content">
                            <p>{{ $informacion -> descripcion}}</p>
                        </div>
                    </section>
 @endforeach
                <!-- Two -->
                    <section id="two" class="wrapper style2">
                        <header>
                            <h2>Desarrollador web</h2>
                            <p>Tecnologo en analisis y desarrollo de sistemas de informacion</p>
                        </header>
                        
                    </section>

                <!-- Three -->
                    

                </div>
            </section>

        <!-- Footer -->
            <footer id="footer">
                <div class="container">
                    <ul class="icons">
                        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                        <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                    </ul>
                </div>
                <div class="copyright">
                    &copy; copyright. todos los derechos reservados. Aprendiz <a href="https://unsplash.com">SENA</a> diseñado  por<a href="https://templated.co">Diego Astaiza</a>
                </div>
            </footer>
            <br>
            <li><a href="http://localhost:8000/index" class="button special small fit"><span class="icon fa-user"> Regresar a inicio</a></li>

        

    </body>
</html>