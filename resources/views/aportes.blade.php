<!DOCTYPE HTML>

<html>
	<head>
		<title>Aportes a proyectos</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link href="{{asset('assets/css/main_aportes.css')}}" rel="stylesheet"/>
	</head>
	<body>

		
			<section id="banner" data-video="images/banner">
				<div class="inner">
					<h1>Aportes a proyectos</h1>
					<p> a continuacion se muestran proyectos en los cuales e participado<br>
					    si requieres mas informacion no dudes en ponerte en contacto conmigo<br>
					    atravez del formulario de contacto o la informacion encontrada en la <br>misma.
					 </p>
					 <a href="{{asset('index')}}">Ir a formulario</a>
					 <a href="https://gitlab.com/users/diego_astaiza">Perfil de git lab</a>
				</div>
			</section>

		
		

		
			<section id="three" class="wrapper style2">
				<div class="inner">
					<div class="grid-style">
  					@foreach ($aportes as $aportes)
						<div>
							<div class="box">
								<div class="image fit">
									<img src="images/w.jpg" alt="" />
								</div>
								<div class="content">
									<header class="align-center">

										<h2>{{$aportes -> titulo}}</h2>
										
									</header>
									<hr />
									<p> {{$aportes -> descripcion}}.</p>
								</div>
							</div>

						</div>
						   @endforeach

						

					</div>
				</div>
			</section>


		
			<section id="four" class="wrapper style3">
				<div class="inner">

					<header class="align-center">
						<h2>Trabajo en equipo</h2>
						<p>“El trabajo en equipo es la capacidad de trabajar juntos hacia una visión común. La capacidad de dirigir los logros individuales hacia los objetivos de la organización. Es el combustible que permite que la gente normal logre resultados poco comunes.</p>
					</header>

				</div>
			</section>

           <a href="http://localhost:8000/index" class="button special small fit"><span class="icon fa-user"> Regresar a inicio</a>

		

		

	</body>
</html>