@extends('layouts.app')

@section('content')
<div class="container">
    {!! Form::open(['route' => ['Informacion.update',$informacion->id], 'method' => 'PUT']) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('nombre', $informacion->nombre, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la peticion']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('descripcion', $informacion->descripcion, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la peticion']) !!}
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                    </div>


@endsection
