<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class portafolio extends Model
{
    protected $table    = "portafolio";
    protected $fillable = ['nombre', 'descripcion', 'id_categoria'];
    protected $guarded  = ["id"];
    public $timestamps  = false;

    public function categoria()
    {

        return $this->belongsTo('App\Models\categoria', 'id_categoria', 'id');
    }
    public function secciones()
    {

        return $this->belongsTo('App\Models\Secciones', 'id_portafolio', 'id');
    }

}
