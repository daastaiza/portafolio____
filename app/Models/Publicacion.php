<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    protected $table    = "publicaciones";
    protected $fillable = ['id', 'id_user', 'titulo', 'contenido', 'id_seccion', 'image'];
    protected $guarded  = ["id"];
    public $timestamps  = false;

    public function Seccion()
    {

        return $this->belongsTo('App\Models\Secciones', 'id_seccion', 'id');
    }
}
