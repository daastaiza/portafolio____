<?php

namespace App\Http\Controllers\Estudios;

use App\Http\Controllers\Controller;
use App\Models\Estudios;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class EstudiosController extends Controller
{
    public function index()
    {
        $estudios = Estudios::all();

        return view('estudios.estudios')->with('estudios', $estudios);
    }

    public function store(Request $request)
    {

        $request->file('image')->store('public');

       
        $estudios = new Estudios($request->all()); 
        $estudios->image = $request->file('image')->store('');
        $estudios->save();

        $estudios = Estudios::all();

        Flash::success('se registro la informacion');

        return view('estudios.estudios')->with('estudios', $estudios);

    }

    public function edit($id)
    {
        $estudios = Estudios::find($id);

        return view('estudios.estudios')->with('estudios', $estudios);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
