<?php

namespace App\Http\Controllers\Portafolio;

use App\Http\Controllers\Controller;
use App\Models\Categoria;
use App\Models\Portafolio;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class PortafioController extends Controller
{
    public function index()
    {
        $portafolio = Portafolio::all();
        $categoria  = Categoria::orderBy('id', 'ASC')->pluck('nombre', 'id')->toArray();

        return view('portafolio.portafolio')
            ->with('categoria', $categoria)
            ->with('portafolio', $portafolio);
    }

    public function store(Request $request)
    {
        $portafolio = new Portafolio($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlog
        $portafolio->save();

        $portafolio = Portafolio::all();

        Flash::success('se registro la informacion');

        return view('portafolio.portafolio')
            ->with('portafolio', $portafolio);

    }

    public function edit($id)
    {
        $portafolio = Portafolio::find($id);

        return view('portafolio.editar')->with('portafolio', $portafolio);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $portafolio = Portafolio::find($id); //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $portafolio->fill($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlogs
        $portafolio->save(); //guardo todo lo que contenga la variable $backlogs

        $portafolio = Portafolio::all();
        return view('portafolio.portafolio')->with('portafolio', $portafolio);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
