<?php

namespace App\Http\Controllers\Ciudad;

use App\Http\Controllers\Controller;
use App\Models\Ciudad;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
class CiudadController extends Controller
{
    public function index()
    {
        $ciudad = Ciudad::all();

        return view('ciudad.ciudad')->with('ciudad', $ciudad);
    }

    public function store(Request $request)
    {
        $ciudad = new Ciudad($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlog
        $ciudad->save(); //guardo todo lo que contenga la variable backlog

        $ciudad = Ciudad::all();
        Flash::success('se registro la informacion');

        return view('ciudad.ciudad')->with('ciudad', $ciudad);
    }

    public function edit($id)
    {
        $ciudad = Ciudad::find($id);

        return view('ciudad.editar')->with('ciudad', $ciudad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $ciudad = Ciudad::find($id); //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $ciudad->fill($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlogs
        $ciudad->save(); //guardo todo lo que contenga la variable $backlogs

        $ciudad = Ciudad::all();
        return view('ciudad.ciudad')->with('ciudad', $ciudad);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
