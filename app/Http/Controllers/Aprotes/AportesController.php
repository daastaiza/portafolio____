<?php

namespace App\Http\Controllers\Aprotes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use App\Models\Aportes;

class AportesController extends Controller
{
    public function index()
    {
        $aportes = Aportes::all();

        return view('aportes.aportes')->with('aportes', $aportes);
    }
     public function store(Request $request)
    {
        $aportes = new Aportes($request->all()); //almaceno todos los datos recibidos de la vista dentro de la variable  $backlog
        $aportes->save(); //guardo todo lo que contenga la variable backlog

        $aportes = Aportes::all();
        Flash::success('se registro la informacion');

        return view('aportes.aportes')->with('aportes', $aportes);
    }
    public function aportes()
    {
        $aportes = Aportes::all();

        return view('aportes')->with('aportes', $aportes);
    }

}
